package services;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import models.UserProfilePhoto;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SaveUserProfilePhotoServiceTest {

	private SaveUserProfilePhotoService service;
	private UserProfilePhoto userProfilePhoto;
	
	@Before
	public void setUp() {
		service = new SaveUserProfilePhotoServiceImpl();
		userProfilePhoto = mock(UserProfilePhoto.class);
	}
	
	@Test
	public void shouldSaveUserProfilePhoto() throws IOException {
		
		service.saveUserProfilePhoto(userProfilePhoto);
		
		verify(userProfilePhoto).save();
	}
	
	@Test
	public void shouldDeleteExistinUserProfilePhotoBeforeSaving() throws IOException {
		
		when(userProfilePhoto.exists()).thenReturn(true);
		
		service.saveUserProfilePhoto(userProfilePhoto);
		
		verify(userProfilePhoto).delete();
		verify(userProfilePhoto).save();
	}
	
	@Test
	public void shouldUpdateUserProfileWhenSavingUserProfilePhoto() {
		
		service.saveUserProfilePhoto(userProfilePhoto);
		
		verify(userProfilePhoto).updateUserProfile(Mockito.anyString());
	}
	
}
