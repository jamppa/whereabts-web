import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import org.fest.assertions.Assertions;
import org.junit.Test;

import play.libs.F.Callback;
import play.test.TestBrowser;


public class LoginIntegrationTest {

	@Test
	public void shouldBrowseToLoginPage() {
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/login");
				Assertions.assertThat(browser.title()).isEqualTo("Whereabts - Login");
			}
		});
	}
}
