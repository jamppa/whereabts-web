package controllers;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.status;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import play.mvc.Result;
import play.test.WithApplication;
import sun.misc.BASE64Encoder;
import util.UserProfilePhotoFixtures;
import util.WhereabtsUserFixtures;

public class UserProfilePhotoControllerTest extends WithApplication {

	@Before
	public void setUp() throws IOException {
		start(fakeApplication());
		WhereabtsUserFixtures.loadFixtures();
		UserProfilePhotoFixtures.loadFixtures();
	}
	
	@Test
	public void shouldReturnCreatedResultWhenPostingUserProfilePhoto() {
	
		Result result = play.test.Helpers.callAction(
				controllers.routes.ref.UserProfilePhotoController.uploadUserProfilePhoto(), 
				fakeRequest()
				.withHeader("authorization", authHeader())
				.withRawBody(new byte[] {}));
				
		assertThat(status(result)).isEqualTo(201);
	}
	
	@Test
	public void shouldReturnOkResultWhenGetingUserProfilePhoto() {
		
		Result result = play.test.Helpers.callAction(
				controllers.routes.ref.UserProfilePhotoController.downloadUserProfilePhoto("jani.jpg"),
				fakeRequest());
		
		assertThat(status(result)).isEqualTo(200);
	}
	
	@Test
	public void shouldReturnNotFoundWhenGetingNonExistingUserProfilePhoto() {
		
		Result result = play.test.Helpers.callAction(
				controllers.routes.ref.UserProfilePhotoController.downloadUserProfilePhoto("some.jpg"),
				fakeRequest());
		
		assertThat(status(result)).isEqualTo(404);
	}
	
	private String authHeader() {
		return "Basic " + new BASE64Encoder().encode("test@user.fi:11-22-33".getBytes());
	}
	
}
