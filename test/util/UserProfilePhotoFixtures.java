package util;

import java.io.File;
import java.io.IOException;

import play.api.Play;

import uk.co.panaxiom.playjongo.PlayJongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;

public class UserProfilePhotoFixtures {

	public static void loadFixtures() throws IOException {
		GridFS userProfilePhotos = new GridFS(PlayJongo.getDatabase(), "profile_photos");
		dropUserProfilePhotos(userProfilePhotos);
		loadUserProfilePhoto(userProfilePhotos);
	}

	private static void loadUserProfilePhoto(GridFS userProfilePhotos) throws IOException {
		File photoFile = Play.getFile("public/images/jani.jpg", Play.current());
		GridFSInputFile gridFSPhotoFile = userProfilePhotos.createFile(photoFile);
		gridFSPhotoFile.setFilename("jani.jpg");
		gridFSPhotoFile.setMetaData(new BasicDBObject("user_id", "123abc"));
		gridFSPhotoFile.save();
	}

	private static void dropUserProfilePhotos(GridFS userProfilePhotos) {
		DBCursor cursor = userProfilePhotos.getFileList();
		while(cursor.hasNext()){
			userProfilePhotos.remove(cursor.next());
		}
	}
	
}
