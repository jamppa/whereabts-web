package util;

import java.util.List;

import models.WhereabtsUserProfile;
import play.libs.Yaml;
import uk.co.panaxiom.playjongo.PlayJongo;

public class WhereabtsUserProfileFixtures {

	@SuppressWarnings("unchecked")
	public static void loadFixtures() {
		List<WhereabtsUserProfile> userProfiles = (List<WhereabtsUserProfile>) Yaml.load("profiles-data.yml");
		loadUserProfiles(userProfiles);
	}

	private static void loadUserProfiles(List<WhereabtsUserProfile> userProfiles) {
		PlayJongo.getCollection("profiles").drop();
		for(WhereabtsUserProfile each : userProfiles){
			PlayJongo.getCollection("profiles").save(each);
		}
	}
	
}
