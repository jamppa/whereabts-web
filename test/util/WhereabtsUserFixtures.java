package util;

import java.util.List;

import models.WhereabtsUser;
import play.libs.Yaml;
import uk.co.panaxiom.playjongo.PlayJongo;

public class WhereabtsUserFixtures {
	
	@SuppressWarnings("unchecked")
	public static void loadFixtures() {
		List<WhereabtsUser> users = (List<WhereabtsUser>) Yaml.load("users-data.yml");
		loadUserFixtures(users);
	}

	private static void loadUserFixtures(List<WhereabtsUser> users) {
		PlayJongo.getCollection("users").drop();
		for(WhereabtsUser each : users){
			PlayJongo.getCollection("users").save(each);
		}
	}

}
