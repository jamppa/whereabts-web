package functional;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import org.junit.Ignore;
import org.junit.Test;

import play.libs.F.Callback;
import play.test.TestBrowser;

public class JoinPilotGroupFunctionalTest {

	@Ignore
	@Test
	public void shouldNavigateToJoinPilotGroupViewFromIndex() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/");
				browser.$("#join-pilot").click();
				assertThat(browser.title()).isEqualTo("Whereabts - Join Pilot");
			}
		});
	}
	
	@Test
	public void shouldNotSubmitWronglyFilledForm() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/join_pilot");
				browser.fill("#companyName").with("Test Oy");
				browser.click("#btn-join-pilot");
				assertThat(browser.$("#error-message").isEmpty()).isFalse();
			}
		});
	}
	
	@Test
	public void shouldSubmitCorrectlyFilledForm() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/join_pilot");
				browser.fill("#companyName").with("Test Oy");
				browser.fill("#contactName").with("Test Man");
				browser.fill("#email").with("test@man.fi");
				browser.click("#btn-join-pilot");
				assertThat(browser.$("#success-message").isEmpty()).isFalse();
			}
		});
	}
	
}
