package functional;

import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import static org.fest.assertions.Assertions.*;
import org.junit.Test;

import play.libs.F.Callback;
import play.test.TestBrowser;

public class JoinBetaGroupFunctionalTest {
	
	@Test
	public void shouldNavigateToJoinBetaGroupViewFromIndex() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/");
				browser.$("#join-beta").click();
				assertThat(browser.title()).isEqualTo("Whereabts - Join in Beta");
			}
		});
	}
	
	@Test
	public void shouldNotSubmitWronglyFilledJoinBetaGroupForm() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/apply_for_beta");
				browser.fill("#name").with("seppo");
				browser.fill("#email").with("");
				browser.click("#btn-join-beta");
				assertThat(browser.$("#error-message").isEmpty()).isFalse();
			}
		});
	}
	
	@Test
	public void shouldSubmitCorrectlyFilledJoinBetaGroupForm() {
		
		running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
			@Override
			public void invoke(TestBrowser browser) throws Throwable {
				browser.goTo("http://localhost:3333/apply_for_beta");
				browser.fill("#name").with("seppo");
				browser.fill("#email").with("seppo@test.fi");
				browser.click("#btn-join-beta");
				assertThat(browser.$("#success-message").isEmpty()).isFalse();
			}
		});
	}
	
}
