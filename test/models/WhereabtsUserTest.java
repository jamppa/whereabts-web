package models;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;

import org.junit.Before;
import org.junit.Test;

import play.test.WithApplication;
import util.WhereabtsUserFixtures;
import util.WhereabtsUserProfileFixtures;
import dtos.EmailAndUuid;

public class WhereabtsUserTest extends WithApplication {

	@Before
	public void setUp() {
		start(fakeApplication());
		WhereabtsUserFixtures.loadFixtures();
		WhereabtsUserProfileFixtures.loadFixtures();
	}
	
	@Test
	public void shouldCountOfUsers() {
		
		long count = WhereabtsUser.count();
		
		assertThat(count).isEqualTo(2);
	}
	
	@Test
	public void shouldAuthenticateUserByEmailAndUuid() {
		
		WhereabtsUser authenticatedUser = 
				WhereabtsUser.authenticate(new EmailAndUuid("test@user.fi", "11-22-33"));
		
		assertThat(authenticatedUser).isNotNull();
	}
	
	@Test
	public void shouldNotAuthenticateUserByEmailAndUuidWhenWrongEmailGiven() {
		
		WhereabtsUser authenticatedUser = 
				WhereabtsUser.authenticate(new EmailAndUuid("some@nonexisting.fi", "11-22-33"));
		
		assertThat(authenticatedUser).isNull();
	}
	
	@Test
	public void shouldNotAuthenticateUserByEmailAndUuidWhenWrongUuidGiven() {
		
		WhereabtsUser authenticatedUser = 
				WhereabtsUser.authenticate(new EmailAndUuid("test@user.fi", "666-666"));
		
		assertThat(authenticatedUser).isNull();
	}
	
	@Test
	public void shouldFindById() {
		
		WhereabtsUser user = WhereabtsUser.findById("52079b52cb949ca1868d2b3f");
		
		assertThat(user).isNotNull();
		assertContents(user);
	}
	
	private void assertContents(WhereabtsUser user) {
		assertThat(user.id).isEqualTo("52079b52cb949ca1868d2b3f");
		assertThat(user.email).isEqualTo("anonymous@whereabts.com");
		assertThat(user.uuid).isEqualTo("abc-123");
		assertThat(user.role).isEqualTo("email");
		assertThat(user.lastSeenAt).isEqualTo(1369228010638L);
		assertThat(user.createdAt).isEqualTo(1369228010638L);
	}

	@Test
	public void shouldNotFindByIdWhenDoesntExist() {
		
		WhereabtsUser user = WhereabtsUser.findById("52079b52cb949ca1868d2666");
		
		assertThat(user).isNull();
	}
	
	@Test
	public void shouldFindProfile() {
		
		WhereabtsUser user = WhereabtsUser.findById("52079b52cb949ca1868d2b3f");
		
		WhereabtsUserProfile profile = user.profile();
		
		assertThat(profile).isNotNull();
	}
	
	@Test
	public void shouldNotFindProfileWhenSuchDoesntExist() {
		
		WhereabtsUser user = new WhereabtsUser();
		
		WhereabtsUserProfile profile = user.profile();
		
		assertThat(profile).isNull();
	}
	
}
