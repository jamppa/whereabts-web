package models;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;

import java.io.File;
import java.io.IOException;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

import play.test.WithApplication;
import util.UserProfilePhotoFixtures;
import util.WhereabtsUserFixtures;
import util.WhereabtsUserProfileFixtures;

import com.mongodb.gridfs.GridFSDBFile;

public class UserProfilePhotoTest extends WithApplication {

	private File somePhotoFile;
	private WhereabtsUser someUser;

	@Before
	public void setUp() throws IOException {
		somePhotoFile = File.createTempFile("photo", ".png");
		someUser = new WhereabtsUser();
		start(fakeApplication());
		WhereabtsUserFixtures.loadFixtures();
		WhereabtsUserProfileFixtures.loadFixtures();
		UserProfilePhotoFixtures.loadFixtures();
	}
	
	@Test
	public void shouldCreateSuchAnObject() throws IOException {
		
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		assertThat(userProfilePhoto.photo).isEqualTo(somePhotoFile);
		assertThat(userProfilePhoto.user).isEqualTo(someUser);
	}
	
	@Test
	public void shouldSaveUserProfilePhotoFile() throws IOException {
		
		someUser.id = new ObjectId().toString();
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		userProfilePhoto.save();
	}
	
	@Test
	public void shouldFindUserProfilePhotoFileByName() throws IOException {
		
		GridFSDBFile photoFile = UserProfilePhoto.findUserProfilePhotoFileByName("jani.jpg");
		
		assertThat(photoFile).isNotNull();
	}
	
	@Test
	public void shouldNotFindUserProfilePhotoFileByNameWhenSuchDoesntExist() {
		
		GridFSDBFile photoFile = UserProfilePhoto.findUserProfilePhotoFileByName("gagga.jpg");
		
		assertThat(photoFile).isNull();
	}
	
	@Test
	public void shouldFindUserProfilePhotoFileByUser() {
		
		GridFSDBFile photoFile = UserProfilePhoto.findUserProfilePhotoFileByUser("123abc");
		
		assertThat(photoFile).isNotNull();
	}
	
	@Test
	public void shouldKnowIfUserProfilePhotoAlreadyExists() throws IOException {
		
		someUser.id = new ObjectId().toString();
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		userProfilePhoto.save();
		
		assertThat(userProfilePhoto.exists()).isEqualTo(true);
	}
	
	@Test
	public void shouldKnowIfUserProfilePhotoDoesntAlreadyExist() {
		
		someUser.id = new ObjectId().toString();
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		assertThat(userProfilePhoto.exists()).isEqualTo(false);
	}
	
	@Test
	public void shouldDeleteUserProfilePhoto() throws IOException {
		
		UserProfilePhoto saved = saveUserProfilePhoto();
		
		saved.delete();
		
		assertThat(saved.exists()).isEqualTo(false);
	}
	
	@Test
	public void shouldDoNothingWhenTryingToDeleteNonExistingUserProfilePhoto() {
		
		someUser.id = new ObjectId().toString();
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		userProfilePhoto.delete();
		
		assertThat(userProfilePhoto.exists()).isEqualTo(false);
	}
	
	@Test
	public void shouldUpdateUsersProfileWithProfilePhotoFilename() throws IOException {
		
		someUser = WhereabtsUser.findById("52079b52cb949ca1868d2b3f");
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		
		String photoFilename = userProfilePhoto.save();
		userProfilePhoto.updateUserProfile(photoFilename);
		
		assertThat(someUser.profile().photo).isEqualTo(photoFilename);
	}

	private UserProfilePhoto saveUserProfilePhoto() throws IOException {
		someUser.id = new ObjectId().toString();
		UserProfilePhoto userProfilePhoto = new UserProfilePhoto(somePhotoFile, someUser);
		userProfilePhoto.save();
		return userProfilePhoto;
	}
	
	
}
