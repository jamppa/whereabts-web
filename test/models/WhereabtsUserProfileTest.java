package models;

import static org.fest.assertions.Assertions.*;
import static play.test.Helpers.fakeApplication;

import org.junit.Before;
import org.junit.Test;

import play.test.WithApplication;
import util.WhereabtsUserProfileFixtures;

public class WhereabtsUserProfileTest extends WithApplication {

	@Before
	public void setUp() {
		start(fakeApplication());
		WhereabtsUserProfileFixtures.loadFixtures();
	}
	
	@Test
	public void shouldFindByWhereabtsUser() {
		
		WhereabtsUserProfile profile = WhereabtsUserProfile.findByUserId("52079b52cb949ca1868d2b3f");
		
		assertThat(profile).isNotNull();
		assertContents(profile);
	}
	
	@Test
	public void shouldNotFindByWhereabtsUserWhenSuchDoesntExist() {
		
		WhereabtsUserProfile profile = WhereabtsUserProfile.findByUserId("520769b0cb949ca1868d2b00");
		
		assertThat(profile).isNull();
	}
	
	@Test
	public void shouldUpdateProfileDetails() {
		
		WhereabtsUserProfile profile = WhereabtsUserProfile.findByUserId("52079b52cb949ca1868d2b3f");
		
		profile.photo = "some.jpg";
		profile.save();
		
		assertThat(WhereabtsUserProfile.findByUserId("52079b52cb949ca1868d2b3f").photo).isEqualTo("some.jpg");
	}

	private void assertContents(WhereabtsUserProfile profile) {
		assertThat(profile.id).isEqualTo("520769b0cb949ca1868d2b3e");
		assertThat(profile.userId).isEqualTo("52079b52cb949ca1868d2b3f");
		assertThat(profile.nick).isEqualTo("jamppa");
		assertThat(profile.country).isEqualTo("fi");
		assertThat(profile.bio).isEqualTo("kowa jätkä");
		assertThat(profile.photo).isNullOrEmpty();
	}
	
}
