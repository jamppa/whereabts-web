package models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AdminUserTest {

	@Test
	public void shouldInstantiate() {
		
		AdminUser user = new AdminUser("jami@arrone.fi", "secret");
		
		assertEquals("jami@arrone.fi", user.email);
		assertEquals("secret", user.password);
	}
	
	@Test
	public void shouldAuthenticateValidUser() {
		
		AdminUser validUser = new AdminUser("jani.arvonen@whereabts.com", "&( =,f{]4#F[Rjf");
		
		boolean isAuthed = validUser.authenticate();
		
		assertTrue(isAuthed);
	}
	
	@Test
	public void shouldNotAuthenticateInvalidUser() {
		
		AdminUser invalidUser = new AdminUser("seppo@muna.fi", "gej");
		
		boolean isAuthed = invalidUser.authenticate();
		
		assertFalse(isAuthed);
	}
	
}
