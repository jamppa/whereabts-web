package models;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import play.libs.Yaml;
import play.test.Helpers;
import play.test.WithApplication;
import uk.co.panaxiom.playjongo.PlayJongo;

public class FeedbackTest extends WithApplication {

	@Before
	public void setUp() {
		start(Helpers.fakeApplication());
		List<Feedback> feedbacks = loadFeedbackFixtures();
		initFixtures(feedbacks, "feedbacks");
	}
	
	private void initFixtures(List<Feedback> feedbacks, String collection) {
		PlayJongo.getCollection(collection).drop();
		for(Object each : feedbacks) {
			PlayJongo.getCollection(collection).save(each);
		}
	}

	@SuppressWarnings("unchecked")
	private List<Feedback> loadFeedbackFixtures() {		
		return (List<Feedback>) Yaml.load("feedbacks-data.yml");
	}

	@Test
	public void shouldFetchAllFeedbackMessagesSortedByTime() {
		
		List<Feedback> feedbacks = Feedback.allFeedbacks();
		
		assertThat(feedbacks.size()).isEqualTo(2);
		assertThatFeedbacksAreMappedCorrectly(feedbacks);
	}

	private void assertThatFeedbacksAreMappedCorrectly(List<Feedback> feedbacks) {
		
		assertNotNull(feedbacks.get(0).id);
		assertEquals("Shitty app guys!", feedbacks.get(0).message);
		assertEquals(1369228010645L, feedbacks.get(0).createdAt);
		
		assertNotNull(feedbacks.get(1).id);
		assertEquals("Nice app guys!", feedbacks.get(1).message);
		assertEquals(1369228010638L, feedbacks.get(1).createdAt);
	}
	
}
