package models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Maps;

import dtos.WhereabtsMessage;

public class WhereabtsMessageTest {

	private JsonNode messageAsJson;
	
	@Before
	public void setUp() throws Exception {
		HashMap<String, Object> jsonMap = setupJsonMap();
		messageAsJson = setupMessageAsJson(jsonMap);
	}

	@Test
	public void shouldConstructWhereabtsMessageFromJson() {
		
		WhereabtsMessage whereabtsMessage = new WhereabtsMessage(messageAsJson);
		
		assertConstructedWhereabtsMessage(whereabtsMessage);
	}
	
	private void assertConstructedWhereabtsMessage(WhereabtsMessage whereabtsMessage) {
		assertThat(whereabtsMessage.id).isEqualTo("123abc");
		assertThat(whereabtsMessage.userId).isEqualTo("abc123");
		assertThat(whereabtsMessage.message).isEqualTo("cool message!");
		assertThat(whereabtsMessage.createdAt).isEqualTo(123456L);
		assertThat(whereabtsMessage.updatedAt).isEqualTo(1234567L);
		assertThat(whereabtsMessage.views).isEqualTo(5);
		assertThat(whereabtsMessage.isOwner).isEqualTo(true);
		assertThat(whereabtsMessage.location.lon).isEqualTo(12.789);
		assertThat(whereabtsMessage.location.lat).isEqualTo(35.123);
	}

	private JsonNode setupMessageAsJson(HashMap<String, Object> jsonMap) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(mapper.writeValueAsString(jsonMap));
	}

	private HashMap<String, Object> setupJsonMap() {
		HashMap<String, Object> jsonMap = Maps.newHashMap();
		jsonMap.put("_id", "123abc");
		jsonMap.put("user_id", "abc123");
		jsonMap.put("nick", "jamppa");
		jsonMap.put("title", "cool title!");
		jsonMap.put("message", "cool message!");
		jsonMap.put("created-at", 123456L);
		jsonMap.put("updated-at", 1234567L);
		jsonMap.put("views", 5);
		jsonMap.put("deleted", true);
		jsonMap.put("owns", true);
		jsonMap.put("loc", new Double[] {12.789, 35.123});
		return jsonMap;
	}

	
}
