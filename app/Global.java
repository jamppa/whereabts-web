import static play.mvc.Results.notFound;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import services.SaveUserProfilePhotoService;
import services.SaveUserProfilePhotoServiceImpl;
import uk.co.panaxiom.playjongo.PlayJongo;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class Global extends GlobalSettings {
	
	private Injector injector = Guice.createInjector(new AbstractModule() {
		@Override
		protected void configure() {
			bind(SaveUserProfilePhotoService.class).to(SaveUserProfilePhotoServiceImpl.class);
		}
	});
	
	@Override
	public void onStart(Application app) {
		if(app.isDev()) {
			Logger.info("using development database: " + PlayJongo.getDatabase().getName());
		}
		
		if(app.isTest()){
			Logger.info("using test database: " + PlayJongo.getDatabase().getName());
			Logger.info("playjongo.test-uri: " + app.configuration().getString("playjongo.test-uri", ""));
		}
	}
		
	@Override
	public Result onHandlerNotFound(RequestHeader arg0) {
		return notFound(views.html.notfound.render());
	}
	
	@Override
	public <A> A getControllerInstance(Class<A> clazz) throws Exception {
		return injector.getInstance(clazz);
	}
	
}
