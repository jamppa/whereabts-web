package api;

public class WhereabtsApi {

	public static final String PUBLIC_USR = "anonymous@whereabts.com";
	public static final String PUBLIC_USR_ID = "ae129325a4db22faab7771f10b39a8af";
	
	public static final String ROOT = "http://api.whereabts.com/api";
	public static final String MESSAGES_API =  ROOT + "/messages/:id";
	
}
