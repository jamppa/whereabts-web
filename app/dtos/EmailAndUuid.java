package dtos;

public class EmailAndUuid {

	public String email;
	public String uuid;
	
	public EmailAndUuid(String email, String uuid) {
		this.email = email;
		this.uuid = uuid;
	}
	
}
