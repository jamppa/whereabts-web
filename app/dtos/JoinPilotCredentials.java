package dtos;

import com.feth.play.module.mail.Mailer.Mail.Body;

import play.data.validation.Constraints.Required;

public class JoinPilotCredentials {

	@Required
	private String companyName;
	@Required
	private String contactName;
	@Required
	private String email;
	
	private String phone;
	
	public JoinPilotCredentials() {
		this.companyName = "";
		this.contactName = "";
		this.email = "";
		this.phone = "";
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Body asMailBody() {
		return new Body(
				"Company: " + this.companyName + "\n" +
				"Contact name: " + this.contactName + "\n" + 
				"Email: " + this.email + "\n" + 
				"Phone: " + this.phone + "\n");
	}
	
	
}
