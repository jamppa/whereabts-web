package dtos;

import com.feth.play.module.mail.Mailer.Mail.Body;

import play.data.validation.Constraints.Required;

public class JoinBetaCredentials {

	@Required
	public String name;
	@Required
	public String email;
	
	public JoinBetaCredentials() {
		this.name = "";
		this.email = "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Body asMailBody() {
		return new Body("Name: " + this.name + "\nEmail: " + this.email);
	}
	
	
	
}
