package dtos;

import models.AdminUser;

public class LoginCredentials {

	public String email;
	public String password;

	public String validate() {
		AdminUser user = new AdminUser(email, password);
		if(user.authenticate()){
			return null;
		}
		return "Invalid email and/or password!";
	}
	
}
