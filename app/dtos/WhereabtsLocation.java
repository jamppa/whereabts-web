package dtos;

import org.codehaus.jackson.JsonNode;

public class WhereabtsLocation {

	public final double lon;
	public final double lat;
	
	public WhereabtsLocation(JsonNode json) {
		this.lon = json.get(0).asDouble();
		this.lat = json.get(1).asDouble();
	}
}
