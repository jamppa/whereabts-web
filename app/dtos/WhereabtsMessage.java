package dtos;


import org.codehaus.jackson.JsonNode;


public class WhereabtsMessage {

	public final String id;
	public final String userId;
	public final String message;
	public final long updatedAt;
	public final long createdAt;
	public final long views;
	public final boolean isOwner;
	public final WhereabtsLocation location;

	public WhereabtsMessage(JsonNode asJson) {
		this.id = asJson.get("_id").getTextValue();
		this.userId = asJson.get("user_id").getTextValue();
		this.message = asJson.get("message").getTextValue();
		this.updatedAt = asJson.get("updated-at").getLongValue();
		this.createdAt = asJson.get("created-at").getLongValue();
		this.views = asJson.get("views").getLongValue();
		this.isOwner = asJson.get("owns").getBooleanValue();
		this.location = new WhereabtsLocation(asJson.get("loc"));
	}
}
