package services;

import models.UserProfilePhoto;

public interface SaveUserProfilePhotoService {

	public void saveUserProfilePhoto(UserProfilePhoto userProfilePhoto);
	
}
