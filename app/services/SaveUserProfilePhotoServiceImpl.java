package services;

import java.io.IOException;

import models.UserProfilePhoto;

public class SaveUserProfilePhotoServiceImpl implements SaveUserProfilePhotoService {

	@Override
	public void saveUserProfilePhoto(UserProfilePhoto userProfilePhoto) {
		if(userProfilePhoto.exists()){
			userProfilePhoto.delete();
		}
		doSave(userProfilePhoto);
	}

	private void doSave(UserProfilePhoto userProfilePhoto) {
		try {
			String photoFilename = userProfilePhoto.save();
			userProfilePhoto.updateUserProfile(photoFilename);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
}
