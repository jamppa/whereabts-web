package controllers;

import dtos.LoginCredentials;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class LoginController extends Controller {

	public static Result login() {
		Form<LoginCredentials> loginForm = Form.form(LoginCredentials.class);
    	return ok(views.html.login.render(loginForm));
    }
	
	public static Result authenticate() {
		Form<LoginCredentials> loginForm = Form.form(LoginCredentials.class).bindFromRequest();
		if(loginForm.hasErrors()){
			return badRequest(views.html.login.render(loginForm));
		}
		return adminViewWithSession(loginForm);
	}
	
	public static Result logout() {
		session().clear();
		return redirect(routes.Application.index());
	}

	private static Result adminViewWithSession(Form<LoginCredentials> loginForm) {
		session().clear();
		session("email", loginForm.get().email);
		return redirect(routes.AdminApplication.index());
	}
}
