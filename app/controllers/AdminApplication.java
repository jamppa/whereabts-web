package controllers;

import models.WhereabtsUser;
import models.Feedback;
import models.view.AdminIndexViewModel;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

@Authenticated(AdminAuthenticator.class)
public class AdminApplication extends Controller {

	public static Result index() {
		AdminIndexViewModel viewModel = 
				new AdminIndexViewModel(Feedback.allFeedbacks(), WhereabtsUser.count());
		return ok(views.html.admin.adminindex.render(viewModel));
	}
	
}
