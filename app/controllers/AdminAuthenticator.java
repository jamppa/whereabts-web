package controllers;

import play.mvc.Result;
import play.mvc.Security;
import play.mvc.Http.Context;

public class AdminAuthenticator extends Security.Authenticator {

	@Override
	public String getUsername(Context context) {
		return context.session().get("email");
	}
	
	@Override
	public Result onUnauthorized(Context context) {
		return redirect(routes.Application.index());
	}
}
