package controllers;

import com.feth.play.module.mail.Mailer;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import dtos.JoinBetaCredentials;

public class JoinBetaGroupController extends Controller {

	public static Result show() {
    	return ok(views.html.applybeta.render(Form.form(JoinBetaCredentials.class)));
    }
	
	public static Result joinBetaGroup() {
		Form<JoinBetaCredentials> joinBetaForm = Form.form(JoinBetaCredentials.class).bindFromRequest();
		if(joinBetaForm.hasErrors()){
			return badRequest(views.html.applybeta.render(joinBetaForm));
		}
		sendMailToWhereabts(joinBetaForm.get());
		flash("success", "Thanks for joining!");
		return redirect(routes.JoinBetaGroupController.show());
	}

	private static void sendMailToWhereabts(JoinBetaCredentials credentials) {
		Mailer.getDefaultMailer().sendMail("Beta user application", 
				credentials.asMailBody(), "whereabts@whereabts.com");
	}
	
}
