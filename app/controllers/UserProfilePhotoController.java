package controllers;

import java.io.File;
import java.io.IOException;

import models.UserProfilePhoto;
import models.WhereabtsUser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.SaveUserProfilePhotoService;
import action.BasicAuth;

import com.google.inject.Inject;
import com.mongodb.gridfs.GridFSDBFile;

public class UserProfilePhotoController extends Controller {

	private final SaveUserProfilePhotoService saveUserProfilePhotoService;
	
	@Inject
	public UserProfilePhotoController(SaveUserProfilePhotoService service) {
		this.saveUserProfilePhotoService = service;
	}
	
	@BasicAuth
	public Result uploadUserProfilePhoto() throws IOException {
		UserProfilePhoto userProfilePhoto = 
				new UserProfilePhoto(uploadedPhotoAsFile(), WhereabtsUser.current(Http.Context.current()));
		saveUserProfilePhotoService.saveUserProfilePhoto(userProfilePhoto);
		return created(userProfilePhoto.asJson());
	}
	
	public Result downloadUserProfilePhoto(String photoFileName) throws IOException {
		GridFSDBFile photoFile = UserProfilePhoto.findUserProfilePhotoFileByName(photoFileName);
		if(photoFile == null){
			return notFound();
		}
		response().setHeader("Content-Length", Long.toString(photoFile.getLength()));
		return ok(photoFile.getInputStream()).as(photoFile.getContentType());
	}

	private static File uploadedPhotoAsFile() {
		return request().body().asRaw().asFile();
	}

}
