package controllers;

import dtos.WhereabtsMessage;
import models.view.WhereabtsMessageViewModel;
import play.libs.F.Function;
import play.libs.WS;
import play.libs.WS.Response;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.notfound;
import views.html.whereabts;
import api.WhereabtsApi;

public class WhereabtsMessageController extends Controller {

	public static Result show(String id) {
		return async(WS.url(new String(WhereabtsApi.MESSAGES_API).replace(":id", id))
				.setAuth(WhereabtsApi.PUBLIC_USR, WhereabtsApi.PUBLIC_USR_ID).get()
				.map(responseToResult()));
	}

	private static Function<Response, Result> responseToResult() {
		return new play.libs.F.Function<WS.Response, Result>() {
			@Override
			public Result apply(Response response) throws Throwable {
				return resultFromResponse(response);
			}};
	}
	
	private static Result resultFromResponse(Response response) {
		if(response.getStatus() != Http.Status.OK){
			return notFound(notfound.render());
		}
		WhereabtsMessageViewModel viewModel = new WhereabtsMessageViewModel(new WhereabtsMessage(response.asJson()));
		return ok(whereabts.render(viewModel));
	}
}
