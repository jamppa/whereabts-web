package controllers;

import com.feth.play.module.mail.Mailer;

import dtos.JoinPilotCredentials;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class JoinPilotGroupController extends Controller {

	public static Result show() {
		return ok(views.html.joinpilot.render(
				Form.form(JoinPilotCredentials.class)));
	}
	
	public static Result joinPilotGroup() {
		Form<JoinPilotCredentials> joinPilotForm = Form.form(JoinPilotCredentials.class).bindFromRequest();
		if(joinPilotForm.hasErrors()){
			return badRequest(views.html.joinpilot.render(joinPilotForm));
		}
		flash("success", "success");
		sendMailToWhereabts(joinPilotForm.get());
		return ok(views.html.joinpilot.render(Form.form(JoinPilotCredentials.class)));
	}

	private static void sendMailToWhereabts(JoinPilotCredentials credentials) {
		Mailer.getDefaultMailer().sendMail(
				"Pilot program application", credentials.asMailBody(), "whereabts@whereabts.com");
	}
	
}
