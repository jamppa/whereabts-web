package controllers;

import play.Play;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.about;
import views.html.index;
import views.html.notfound;
import views.html.press;
import views.html.privacy;
import views.html.terms;

public class Application extends Controller {
  
    public static Result index() {
        return ok(index.render());
    }
    
    public static Result about() {
    	return ok(about.render());
    }

    public static Result press() {
	return ok(press.render());
    }

    public static Result privacy() {
        return ok(privacy.render());
    }

    public static Result terms() {
        return ok(terms.render());
    }

    public static Result notfound() {
        return ok(notfound.render());
    }
    
    public static Result download() {
    	return redirect(downloadUri());
    }

	private static String downloadUri() {
		return Play.application()
				.configuration().getString("whereabts.download.uri", "");
	}
    
}
