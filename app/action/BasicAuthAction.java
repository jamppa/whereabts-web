package action;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import models.WhereabtsUser;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;
import sun.misc.BASE64Decoder;

import com.google.common.base.Strings;

import dtos.EmailAndUuid;

public class BasicAuthAction extends Action.Simple {

	@Override
	public Result call(Context ctx) throws Throwable {
		EmailAndUuid emailAndUuid = decodeBasicAuth(ctx);
		if(emailAndUuid == null){
			return unauthorizedWithHeader(ctx);
		}
		return authenticationResult(emailAndUuid, ctx);
	}

	private Result authenticationResult(EmailAndUuid emailAndUuid, Context ctx) throws Throwable {
		WhereabtsUser user = WhereabtsUser.authenticate(emailAndUuid);
		if(user == null){
			return unauthorizedWithHeader(ctx);
		}
		ctx.args.put("user", user);
		return delegate.call(ctx);
	}

	private Result unauthorizedWithHeader(Context ctx) {
		ctx.response().setHeader("WWW-Authenticate", "Basic realm=\"Unauthorized!\"");
		return unauthorized();
	}

	private EmailAndUuid decodeBasicAuth(Context ctx) throws UnsupportedEncodingException, IOException {
		String authHeader = ctx.request().getHeader("authorization");
		if(Strings.isNullOrEmpty(authHeader)){
			return null;
		}
		return extractEmailAndUuid(authHeader.replaceFirst("Basic ", ""));
	}

	private EmailAndUuid extractEmailAndUuid(String basicAuthStr) throws UnsupportedEncodingException, IOException {
		String decoded = new String(new BASE64Decoder().decodeBuffer(basicAuthStr), "UTF-8");
		String[] emailAndUuid = decoded.split(":");
		if(emailAndUuid == null || emailAndUuid.length != 2){
			return null;
		}
		return new EmailAndUuid(emailAndUuid[0], emailAndUuid[1]);
	}

}
