package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.Id;

import uk.co.panaxiom.playjongo.PlayJongo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

public class Feedback {

	@Id
	public ObjectId id;
	
	@JsonProperty("user_id")
	public ObjectId userId;
	
	@JsonProperty("message")
	public String message;
	
	@JsonProperty("vote")
	public long vote;
	
	@JsonProperty("created-at")
	public long createdAt;
	
	public Feedback() {}
	
	public static MongoCollection feedbacks() {
		return PlayJongo.getCollection("feedbacks");
	}
	
	public static List<Feedback> allFeedbacks() {
		return Lists.newArrayList(
				feedbacks().find().sort("{created-at : -1}").as(Feedback.class));
	}
	
	public String timestamp() {
		return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date(createdAt));
	}
	
}
