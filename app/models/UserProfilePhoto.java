package models;

import java.io.File;
import java.io.IOException;

import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

import uk.co.panaxiom.playjongo.PlayJongo;

import com.mongodb.BasicDBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class UserProfilePhoto {

	public final File photo;
	public final WhereabtsUser user;
	
	public UserProfilePhoto(final File photo, final WhereabtsUser user) {
		this.photo = photo;
		this.user = user;
	}
	
	public String save() throws IOException {
		String photoFileName = userProfilePhotoFilename();
		GridFSInputFile gridFSPhotoFile = userProfilePhotos().createFile(photo);
		gridFSPhotoFile.setFilename(photoFileName);
		gridFSPhotoFile.setContentType("image/jpeg");
		gridFSPhotoFile.setMetaData(new BasicDBObject("user_id", user.id));
		gridFSPhotoFile.save();
		return photoFileName;
	}
	
	public void updateUserProfile(String photoFilename) {
		WhereabtsUserProfile profile = user.profile();
		if(profile != null){
			updatePhotoFilenameToProfile(profile, photoFilename);
		}
	}
		
	private void updatePhotoFilenameToProfile(WhereabtsUserProfile profile, String photoFilename) {
		profile.photo = photoFilename;
		profile.save();
	}

	public void delete() {
		userProfilePhotos().remove(
				findUserProfilePhotoFileByUser(user.id));
	}
	
	private String userProfilePhotoFilename() {
		return new ObjectId().toString() + ".jpg";
	}

	public JsonNode asJson() {
		ObjectNode json = play.libs.Json.newObject();
		json.put("photo", "created");
		return json;
	}

	public boolean exists() {
		return findUserProfilePhotoFileByUser(user.id) != null;
	}

	public static GridFSDBFile findUserProfilePhotoFileByName(String profilePhotoFileName) {
		GridFSDBFile photoFile = userProfilePhotos().findOne(profilePhotoFileName);
		if(photoFile != null){
			return photoFile;
		}
		return null;
	}
	
	public static GridFSDBFile findUserProfilePhotoFileByUser(String userId) {
		GridFSDBFile photoFile = userProfilePhotos().findOne(new BasicDBObject("metadata.user_id", userId));
		if(photoFile != null){
			return photoFile;
		}
		return null;
	}
	
	private static GridFS userProfilePhotos() {
		return new GridFS(PlayJongo.getDatabase(), "profile_photos");
	}

}
