package models;

import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.Id;
import org.jongo.marshall.jackson.oid.ObjectId;

import play.mvc.Http.Context;
import uk.co.panaxiom.playjongo.PlayJongo;

import com.fasterxml.jackson.annotation.JsonProperty;

import dtos.EmailAndUuid;

public class WhereabtsUser {
	
	@Id @ObjectId
	public String id;
	
	@JsonProperty("user-uuid")
	public String uuid;
	
	@JsonProperty("email")
	public String email;
	
	@JsonProperty("created-at")
	public long createdAt;
	
	@JsonProperty("last-seen-at")
	public long lastSeenAt;
	
	@JsonProperty("role")
	public String role;

	public WhereabtsUser() {}
	
	public static MongoCollection users() {
		return PlayJongo.getCollection("users");
	}
	
	public static long count() {
		return users().count();
	}

	public static WhereabtsUser current(Context ctx){
		return (WhereabtsUser) ctx.args.get("user");
	}
	
	public static WhereabtsUser authenticate(EmailAndUuid emailAndUuid) {
		return users().findOne("{email:#, user-uuid:#}", 
				emailAndUuid.email, emailAndUuid.uuid).as(WhereabtsUser.class);
	}

	public static WhereabtsUser findById(String id) {
		return users().findOne(new org.bson.types.ObjectId(id)).as(WhereabtsUser.class);
	}

	public WhereabtsUserProfile profile() {
		if(org.bson.types.ObjectId.isValid(id)){
			return WhereabtsUserProfile.findByUserId(id);
		}
		return null;
	}
		
}
