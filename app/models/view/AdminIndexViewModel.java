package models.view;

import java.util.List;

import com.google.common.collect.Lists;

import models.Feedback;

public class AdminIndexViewModel {

	private final List<Feedback> feedbacks;
	private final long userCount;
	
	public AdminIndexViewModel(List<Feedback> feedbacks, long userCount) {
		this.feedbacks = Lists.newArrayList(feedbacks);
		this.userCount = userCount;
	}
	
	public String userCount() {
		return String.valueOf(userCount);
	}
	
	public final List<Feedback> feedbacks() {
		return feedbacks;
	}
	
}
