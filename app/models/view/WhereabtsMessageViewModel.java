package models.view;

import java.util.Date;
import java.util.Locale;


import org.ocpsoft.prettytime.PrettyTime;

import dtos.WhereabtsMessage;

public class WhereabtsMessageViewModel {
	
	private final WhereabtsMessage whereabtsMessage;
	
	public WhereabtsMessageViewModel(WhereabtsMessage message) {
		this.whereabtsMessage = message;
	}
	
	public String message() {
		return whereabtsMessage.message;
	}
	
	public double latitude() {
		return whereabtsMessage.location.lat;
	}
	
	public double longitude() {
		return whereabtsMessage.location.lon;
	}
	
	public String whenPosted() {
		PrettyTime p = new PrettyTime(new Locale(""));
		return p.format(new Date(whereabtsMessage.createdAt));
	}

	public String title() {
		return whereabtsMessage.message;
	}
	
}
