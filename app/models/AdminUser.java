package models;

import java.util.ArrayList;
import java.util.List;

public class AdminUser {

	private final static List<AdminUser> validUsers = setupValidUsersList();
	
	public String email;
	public String password;
	
	public AdminUser(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public boolean authenticate() {
		return validUsers.contains(this);
	}

	private static List<AdminUser> setupValidUsersList() {
		List<AdminUser> users = new ArrayList<AdminUser>();
		users.add(new AdminUser("jani.arvonen@whereabts.com", "&( =,f{]4#F[Rjf"));
		users.add(new AdminUser("antti.siukola@whereabts.com", "&( =,f{]4#F[Rjf"));
		return users;
	}
	

	@Override
	public boolean equals(Object obj) {
		AdminUser another = (AdminUser) obj;
		return (another.email.equals(this.email) &&
				another.password.equals(this.password));
	}
}
