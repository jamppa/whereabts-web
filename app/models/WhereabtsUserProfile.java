package models;

import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.Id;
import org.jongo.marshall.jackson.oid.ObjectId;

import uk.co.panaxiom.playjongo.PlayJongo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WhereabtsUserProfile {
	
	@Id @ObjectId
	public String id;
	
	@ObjectId
	@JsonProperty("user_id")
	public String userId;
	
	@JsonProperty("nick")
	public String nick;
	
	@JsonProperty("description")
	public String bio;
	
	@JsonProperty("country")
	public String country;
	
	@JsonProperty("photo")
	public String photo;
	
	public WhereabtsUserProfile() {}

	public static WhereabtsUserProfile findByUserId(String userId) {
		return profiles().findOne("{user_id:#}", new org.bson.types.ObjectId(userId))
				.as(WhereabtsUserProfile.class);
	}
	
	private static MongoCollection profiles() {
		return PlayJongo.getCollection("profiles");
	}

	public void save() {
		profiles().save(this);
	}
	
}
