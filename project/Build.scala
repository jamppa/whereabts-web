import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "whereabts-web"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
   
    javaCore,
    javaJdbc,
    javaEbean,
    "com.google.inject" % "guice" % "3.0",
    "javax.inject" % "javax.inject" % "1",
    "uk.co.panaxiom" %% "play-jongo" % "0.4",
    "org.ocpsoft.prettytime" % "prettytime" % "3.0.2.Final",
    "org.mockito" % "mockito-core" % "1.9.5",
    "com.feth" %% "play-easymail" % "0.2-SNAPSHOT"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
  
  	javaOptions in Test += "-Dconfig.file=conf/application_test.conf",
	
	resolvers += Resolver.url("My GitHub Play Repository", url("http://alexanderjarvis.github.com/releases/"))(Resolver.ivyStylePatterns),
    resolvers += "sonatype-snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
    resolvers += Resolver.url("play-easymail (release)", url("http://joscha.github.com/play-easymail/repo/releases/"))(Resolver.ivyStylePatterns),
    resolvers += Resolver.url("play-easymail (snapshot)", url("http://joscha.github.com/play-easymail/repo/snapshots/"))(Resolver.ivyStylePatterns)
  )

}
